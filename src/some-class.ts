export class SomeClass {
    constructor() {}
    
    getGenericGreeting() {
        return 'Hello!';
    }

    getPersonalGreeting(name: string) {
        return 'Hello, person!';
    }
}

import { SomeClass } from '../src/some-class'

describe('Some class', () => {
    let systemUnderTest: SomeClass;

    beforeEach(() => {
        systemUnderTest = new SomeClass();
    });

    it('Should generically greet everyone', () => {
        const greeting = systemUnderTest.getGenericGreeting();

        expect(greeting).toBe('Hello!');
    });

    it('Should greet person by name', () => {
        const greeting = systemUnderTest.getPersonalGreeting('person');

        expect(greeting).toBe('Hello, person!');
    });
});
